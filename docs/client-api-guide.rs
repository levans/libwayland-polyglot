//! This is a proposal of low-level wayland client API taylored for cross-language bindings.
//! The whole usual C API can mostly be built on top of it (this API os more strict than the C
//! one on some points), but it is more friendly to other languages (notably Rust) as well.
//!
//! The end goal would be the creation of a rust library (build on my current wayland-rs) that
//! exposes both the current C API of libwayland-client.so and this wlp_ API, allowing easier
//! interop between different languages in the same application.
//!
//! The main driving points were to make the API as small and generic as possible (it is
//! not meant for general use), and so that it should never free behind your back a pointer
//! that was given to you (hence the use of recounting). Also, it avoids using global state
//! (including touching errno) for cross-language friendlyness. Thread-safety is explicitly
//! specified. Assumptions about the allocator are also avoided, hence no ownership of
//! allocation crosses the API.
//!
//! This API uses the same `wl_interface` and `wl_argument` and `wl_array` types as the current
//! C API, as well as the `wl_array` manipulation functions (init/release/add/copy).
//!
//! "string" arguments are always represented as bytes `wl_array` of their content in `wl_argument`
//! instances accross this API (just like at the wire level).

/*
 * Display-related types and functions
 */

/// An opaque type representing the client Display, handle to the connection.
///
/// The underlying type is thread-safe, and pointers to it can be shared between threads.
struct wlp_client_display;

/// Initialize a new Wayland connection from a connected unix socket
///
/// Higher-level apis are responsible for the mechanism of finding and opening the socket.
extern "C" unsafe fn wlp_client_display_from_fd(fd: RawFd) -> *mut wlp_client_display;

/// Access the connection file descriptor.
///
/// You should not attempt to read from or write to it, as it still belong to wlp. You can
/// use it to register interest in poll/epoll or similar mechanisms.
extern "C" unsafe fn wlp_client_display_get_fd(display: *mut wlp_client_display) -> RawFd;

/// Flush the outgoing buffer
///
/// Empty the outgoing buffer into the socket. Never blocks but can fail with EAGAIN if
/// writing would be blocking. Returns the number of flushed messages on success and
/// `-errno` on failure.
extern "C" unsafe fn wlp_client_display_flush(display: *mut wlp_client_display) -> c_int;

/// Increase the display refcount
///
/// The *mut wlp_client_display pointers are atomically reference counted pointer, this
/// function creates a new reference-counted handle.
extern "C" unsafe fn wlp_client_display_clone(display: *mut wlp_client_display) -> *mut wlp_client_display;

/// Release this display refcount handle
///
/// This decreases the refcount and the provided pointer should no longer be used. When
/// all handles are destroyed (including those potentially owned by other types from this
/// library) the connection is automatically closed.
extern "C" unsafe fn wlp_client_display_release(display: *mut wlp_client_display);

/// Disconnect the wayland connection
///
/// This does not free the resources, and you still need to call the appropriate `*_release`
/// functions.
extern "C" unsafe fn wlp_client_display_disconnect(display: *mut wlp_client_display);

/// Retrieve the last IO error that occurred on the connection
///
/// Such errors are fatal to the wayland connection. Returns `0` if no fatal error has occurred, and
/// `errno` if any has occurred. If this function returns a non-zero value, the connection is
/// already closed.
///
/// If instead a protocol error occurred, returns `EPROTO`.
extern "C" unsafe fn wlp_client_display_get_io_error(display: *mut wlp_client_display) -> c_int; 

/// Retrieve the last protocol error that occurred on the Wayland connection.
///
/// Returns -1 if no error occured, the error code otherwise. If this function returns
/// a positive value, the second argument pointer will be filled with a pointer to the
/// interface of the object that generated the error, and the third argument pointer
/// with the id of the object that generated the error.
extern "C" unsafe fn wlp_client_display_get_protocol_error(
    display: *mut wlp_client_display,
    interface: *mut *const wl_interface
    id: *mut u32) -> c_int;

/// Retrieve the last protocol error message.
///
/// If a protocol error occured and contained a message, retrieve it. In this case
/// this function stores it in the provided bytes array and returns 0. Returns -1 otherwise.
extern "C" unsafe fn wlp_client_display_get_error_message(display: *mut wlp_client_display, error: *mut wl_array) -> c_int;

/// Create a new event queue associated with this wayland connection.
///
/// The original `wl_display` object will automatically be registered
/// to the first created queue.
extern "C" unsafe fn wlp_client_display_create_queue(display: *mut wlp_client_display) -> *mut wlp_client_event_queue;

/// Get the proxy associated to the `wl_display` wayland object.
///
/// This increases the refcount associated with the proxy.
extern "C" unsafe fn wlp_client_display_get_proxy(display: *mut wlp_client_display) -> *mut wlp_client_proxy;

/*
 * Functions and types associated with the event queues
 */

/// An event queue
///
/// This type is *not* threadsafe, and should *not* be shared between threads.
struct wlp_client_queue;

/// A structure representing an event
struct wlp_client_event {
    proxy: *mut wlp_client_proxy,  // target object for the event, without increased refcount
    interface: *const wl_interface, // the interface of this proxy
    opcode: u16,                    // opcode of the received event
    arguments: *const wl_argument,  // array of arguments for this event
    data1: *mut c_void,             // First user-data pointer of this proxy
    data2: *mut c_void,             // Second used-data pointer of this proxy
}

/// Dispatch the pending messages for this queue
///
/// Never blocks. Calls the dispatcher of pending messages until either:
///
/// - the queue is empty, in which case it'll return the number of dispatched messages
/// - it reaches a message that has no dispatcher, in which case it'll return -1
extern "C" unsafe fn wlp_client_queue_dispatch_pending(queue: *mut wlp_client_queue) -> c_int;

/// Retrieve the next event for manual processing.
///
/// Return -1 if the next event of this queue is for an object with an implementation, in which
/// case you need to call `wlp_client_queue_dispatch_pending`. If a message is available for
/// manual processing, returns `0` and fills in the `event` argument with the event, that'll
/// then need to free using `wlp_client_free_event`.
///
/// To ensure proper freeing of the received event, you should not modify it, and treat it
/// as read-only.
extern "C" unsafe fn wlp_client_queue_next_event(
    queue: *mut wlp_client_queue,
    event: *mut wlp_client_event,
) -> c_int;

/// Free a previously obtained event.
extern "C" unsafe fn wlp_client_free_event(event: *mut wlp_client_event);

/// Increase the queue refcount
///
/// The *mut wlp_client_queue pointers are reference counted pointer, this
/// function creates a new reference-counted handle.
extern "C" unsafe fn wlp_client_queue_clone(queue: *mut wlp_client_queue) -> *mut wlp_client_queue;

/// Release this queue refcount handle
///
/// This decreases the refcount and the provided pointer should no longer be used. When
/// all handles are destroyed (including those potentially owned by other types from this
/// library) the queue is automatically destroyed.
extern "C" unsafe fn wlp_client_queue_release(queue: *mut wlp_client_queue);

/// Prepare a read of the socket for new events.
///
/// Returns `-1` if there are pending events in this queue that need to be processed.
///
/// On success this must be followed by a call to `wlp_client_queue_read_events` or
/// `wlp_client_queue_cancel_read` before any dispatching is attempted.
extern "C" unsafe fn wlp_client_queue_prepare_read(queue: *mut wlp_client_queue) -> c_int;

/// Read events from the socket and buffer them in the appropriate queue buffers.
///
/// Must be called after `wlp_client_queue_prepare_read` returning `0`, will do nothing
/// and return `-EPERM` if this is not the case. On success returns the number of read
/// events (may be 0 if events were conccurently read by an other thread), on IO error
/// returns the appropriate `-errno`.
extern "C" unsafe fn wlp_client_queue_read_events(queue: *mut wlp_client_queue) -> c_int;

/// Cancel a prepared read of the socket.
extern "C" unsafe fn wlp_client_queue_cancel_read(queue: *mut wlp_client_queue);

/*
 * Proxy-related types and functions
 */

/// An opaque type representing a proxy.
///
/// It is a threadsafe refcounted handle.
struct wlp_client_proxy;

type wlp_client_dispatcher_t = extern "C" unsafe fn(*const wlp_client_event) -> c_int;

type wlp_client_cleanup_t = extern "C" unsafe fn(data1: *mut c_void, data2: *mut c_void);

/// Implement a proxy by providing a dispatcher function for incoming events.
///
/// By default, newly created-proxies are considered as "manually" dispatched, meaning their
/// events must be processed using `wlp_client_queue_next_event`. This functions registers
/// a callback function that will be called to handle the events instead. You can call this
/// function more than once to overwrite a previous dispatcher. Providing a NULL dispatcher
/// will mark the proxy as being manually dispatched.
///
/// You can associate two user_data pointers with this object as well, which will both
/// be given back to you callback whenever an event is received. Having two data pointers
/// rather than a single is done for convenience purposes.
///
/// Your dispatcher functions returning a non-zero error code will be treated as a
/// fatal protocol error.
///
/// If not NULL, the provided `cleanup` function will be called when the proxy is destroyed
/// for you to free your user data if necessary.
///
/// THREAD-SAFETY: This function should only be called from the same thread as the one owning the
/// event queue the proxy is registered to. If called from an other thread, it'll do nothing and
/// return -1.
extern "C" unsafe fn wlp_client_proxy_implement(
  new_proxy: *mut wlp_client_proxy,
  callback: wlp_client_dispatcher_t,
  data1: *mut c_void,
  data2: *mut c_void,
  cleanup: wlp_client_cleanup_t,
) -> c_int;

/// Get the protocol ID of this wayland object
///
/// Returns -1 if the object is no longer alive.
extern "C" unsafe fn wlp_client_proxy_id(proxy: *mut wlp_client_proxy) -> c_int;

/// Get the interface of this wayland object
extern "C" unsafe fn wlp_client_proxy_interface(proxy: *mut wlp_client_proxy) -> *const wl_interface;

/// Compare two interfaces for equality
///
/// Returns 1 if they are the same interface, 0 otherwise.
///
/// Comparing interfaces by simple pointer equality can spuriously fail if more than one
/// instance of the interface definitions have been linked into the final program (which
/// can occur when linking to wayland-aware libraries for example).
extern "C" unsafe fn wlp_client_compare_interfaces(
    interface1: *const wl_interface,
    inferface2: *const wl_interface
) -> c_int;

/// Get the version of this wayland object
extern "C" unsafe fn wlp_client_proxy_version(proxy: *mut wlp_client_proxy) -> u32;

/// Increase the refcount and create a new handle
extern "C" unsafe fn wlp_client_proxy_clone(proxy: *mut wlp_client_proxy) -> *mut wlp_client_proxy;

/// Decrease the refcount and release a handle
extern "C" unsafe fn wlp_client_proxy_release(proxy: *mut wlp_client_proxy);

/// Destroy this protocol object
///
/// This should only be done in response to a destructor event or following a destructor request.
/// Failure to do so, or calling this function at an innapropriate time may corrupt the internal
/// protocol state.
///
/// This does *not* decrease the refcount, you are reponsible for calling `wlp_client_proxy_release`
/// once you are finished with the proxy. As such you can still access the user data after having
/// destroyed the protocol object.
///
/// Once a protocol object is destroyed, its associated proxy is marked as no longer alive, and will
/// be automatically freed once its refcount reaches zero.
extern "C" unsafe fn wlp_client_proxy_destroy(proxy: *mut wlp_client_proxy);

/// Create a wrapper for this proxy.
///
/// This is similar to increasing the refcount, but the new handle can be associated
/// to an other event queue, which its children objects will inherit.
///
/// This does not affect the queue used to dispatch current object's events.
///
/// Just like other handles, wrappers count towards the refcount of a proxy.
extern "C" unsafe fn wlp_client_proxy_make_wrapper(proxy: *mut wlp_client_proxy) -> *mut wlp_client_proxy;

/// Send a request to this object
///
/// Send given requests to this object. If the request creates an object it given as a return value,
/// if not returns NULL.
extern "C" unsafe fn wlp_client_proxy_send(
    proxy: *mut wlp_client_proxy,
    opcode: u16,
    arguments: *mut wl_argument, // argument array, with placeholder NULL on the newid argument if any
                                 // NOTE: this function will *not* take ownership of the argument array,
                                 // so you'll need to free it yourself
    new_interface: *const wl_interface, // interface of the created object, should only be non-null for
                                        // generic creation requests like wl_registry.bind
    new_version: u32, // version of the created object, should only be non-zero for generic creation
                      // requests like wl_registry.bind
) -> *mut wlp_client_new_proxy;

/// User-data struct
#[repr(C)]
struct wlp_client_user_data {
    pub data1: *mut c_void,
    pub data2: *mut c_void
}

/// Access a proxy's user data
///
/// These are the same pointers as the ones you provided during the implementation and are given to your dispatcher.
///
/// Access to the pointers is not synchronized, as such it is not recommended to modify them after initial
/// implementation. You are responsible for adding synchronization as needed.
///
/// The provided pointer is only valid as long as the input proxy pointer is.
extern "C" unsafe fn wlp_client_proxy_user_data(proxy: *mut wlp_client_proxy) -> *mut wlp_client_user_data;

