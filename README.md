# libwayland-polyglot

Rust implementation of libwayland, providing an alternate API tailored for FFI.

**This is a work in progress, currently at the design & prototype level.**

## Aims

This project aims at the creation of a set of libraries
`libwayland-polyglot-{client,server}.so` that implement in Rust the serialization
of the Wayland protocol both clients-ide and server-side, but exposing an API
tailored for being used by FFi bindings.

In particular, it puts strong emphasis on following these principles:

- the caller language controls the lifetime of all objects (no pointer is implicitly
  freed behind your back)
- thread-safety guarantees are specified and explicited
- global state is not touched (returning error codes rather than accessing errno)
- ownership of allocations does not cross FFI boundary (malloc & free are always
  called by the same side for a given pointer)

Activating the `capi` cargo feature will additionnaly make the libraries expose
the same symbols as `libwayland-{client,server}.so`, allowing them to be used as
drop-in replacement for the system-wide wayland libraries.

## Organisation of the repository

The content of the repository is split into several crates:

- `wlp-commons` contains all structures & functions that are shared between client
  side and server side
- `wlp-client` contains the code for creating `libwayland-polyglot-client.so`
- `wlp-server` contains the code for creating `libwayland-polyglot-server.so`

## Status

Currently the project is at the "API design" phase, to create a wayland serialization
API that is tailored for FFI use.
